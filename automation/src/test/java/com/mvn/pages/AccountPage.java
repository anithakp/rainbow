package com.mvn.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mvn.generics.FUtill;

public class AccountPage extends FUtill
{
	@FindBy(xpath="(//a[@title='T-shirts'])[2]")
	private WebElement Tshirtstab;
	
	public AccountPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void clickonTshirtstab()
	{
		Tshirtstab.click();
	}
		


}
