package com.mvn.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import com.mvn.generics.FUtill;

public class TshirtPage extends FUtill {
	@FindBy(xpath = "//img[@title='Faded Short Sleeve T-shirts']")
	private WebElement FadedTshirt;
	@FindBy(xpath = "//span[@class='available-now']")
	private WebElement Instock;
	@FindBy(css = "a[title='Add to cart']")
	private WebElement addtocartBTN;
	@FindBy(xpath = "//a[@title='Proceed to checkout']")
	private WebElement checkoutBTN;
	@FindBy(xpath = "//h2[contains(.,' successfully added to your')]")
	private WebElement msg;
	@FindBy(xpath = "//div[@class='layer_cart_product_info']")
	private WebElement msg1;
	@FindBy(xpath = "//span[@class='product-name']")
	private WebElement msg2;
	@FindBy(xpath = "//span[@id='layer_cart_product_attributes']")
	private WebElement msg3;
	@FindBy(xpath = "(//strong[@class='dark'])[1]")
	private WebElement msg4;
	@FindBy(xpath = "//span[@id='layer_cart_product_quantity']")
	private WebElement msg5;
	@FindBy(xpath = "(//strong[@class='dark'])[1]")
	private WebElement msg6;
	@FindBy(xpath = "//span[@id='layer_cart_product_price']")
	private WebElement msg7;

	// @FindBy(xpath="//span[text()='Faded Short Sleeve T-shirts']")
	// private WebElement info1;
	//
	public void verify(WebDriver driver) {
		String actualinfo = msg.getText();
		// Assert.assertEquals(actualinfo,
		// "Product successfully added to your shopping cart");
		Reporter.log("The Information is Verified", true);
		Reporter.log(msg1.getText(), true);
		Reporter.log(msg2.getText(), true);
		Reporter.log(msg3.getText(), true);
		Reporter.log(msg4.getText(), true);
		Reporter.log(msg5.getText(), true);
		Reporter.log(msg6.getText(), true);
		Reporter.log(msg7.getText(), true); }
	
	public void instock(WebDriver driver) {
		Actions a = new Actions(driver);
		a.moveToElement(Instock).build().perform();
	}

	public TshirtPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void clickonFadedTshirt() {
		FadedTshirt.click();
	}

	public void Checkout() {
		checkoutBTN.click();
	}

	public void clickonaddTocartBTN() {
		addtocartBTN.click();
	}

}
