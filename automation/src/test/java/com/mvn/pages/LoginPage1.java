package com.mvn.pages;

import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mvn.generics.FUtill;

public class LoginPage1 extends FUtill
{

	@FindBy(xpath = "//a[@class='login']")
	private WebElement signIn;

	@FindBy(xpath="(//input[@name='email'])[1]")
	private WebElement unTB;

	@FindBy(css="input[name='passwd']")
	private WebElement pwTB;

	@FindBy(xpath="//button[@name='SubmitLogin']")
	private WebElement signInBTN;

	public LoginPage1(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void clickOnSignIN() {
		signIn.click();
	}

	public void setUN(String un) {
		unTB.sendKeys(un);
	}

	public void setPW(String pw) {
		pwTB.sendKeys(pw);
	}

	public void clickOnSignINBTN() {
		signInBTN.click();
	}

	public void Login(String un, String pw) {
		unTB.sendKeys(un);
		pwTB.sendKeys(pw);
	}
}
