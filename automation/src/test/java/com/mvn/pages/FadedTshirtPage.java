package com.mvn.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mvn.generics.FUtill;

public class FadedTshirtPage  extends FUtill

{
	
//	@FindBy(xpath="//span[text()='Add to cart']/..")
	@FindBy(css="a[title='Add to cart']")
	private WebElement addtocartBTN;
	
//	@FindBy(xpath="//a[@class='color_pick selected']")
//	private WebElement selectcolor;

//	//@FindBy(xpath="//button[@name='Submit']")
	@FindBy(xpath="(//a[@title='Close']")
	private WebElement closeBTN;
	
	
	public FadedTshirtPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
		
	public void close()
	{
		closeBTN.click();
	}

	public void clickonaddTocartBTN()
	{
		addtocartBTN.click();
	}
	
}
