package com.mvn.scripts;

import org.testng.annotations.Test;

import com.mvn.generics.Basetest;
import com.mvn.pages.AccountPage;
import com.mvn.pages.LoginPage1;
import com.mvn.pages.TshirtPage;

public class LoginTest extends Basetest 
{
	@Test
	public void testLoginPage() throws InterruptedException 
	{
		LoginPage1 lp = new LoginPage1(driver);
		lp.clickOnSignIN();
		Thread.sleep(3000);
		lp.Login(UN, PW);
		Thread.sleep(1000);
		lp.clickOnSignINBTN();

		AccountPage ap = new AccountPage(driver);
		ap.verifyPage(driver, "My account - My Store");
		ap.clickonTshirtstab();
		Thread.sleep(1000);

		TshirtPage tp = new TshirtPage(driver);
		Thread.sleep(3000);
		tp.instock(driver);
		tp.clickonaddTocartBTN();
		tp.verify(driver);
		tp.Checkout();
	}
}
