package com.mvn.generics;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

public class FUtill {
	public void takeScreenShot(WebDriver driver, String path) {
		try {

			TakesScreenshot ts = (TakesScreenshot) driver;
			File src = ts.getScreenshotAs(OutputType.FILE);
			File des = new File(path);
			FileUtils.copyFile(src, des);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyPage(WebDriver driver, String expectedTitle) {

		String actualTitle = driver.getTitle();
		Assert.assertEquals(actualTitle, expectedTitle);
		Reporter.log("The Page is Verified--->" + expectedTitle, true);

	}

}
