package com.mvn.generics;

import java.util.concurrent.TimeUnit; 

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class Basetest extends FUtill implements Iconstants 
{
	static
	{
		System.setProperty(CHROME_KEY,CHROME_VALUE);
		System.setProperty(GECKO_KEY,GECKO_VALUE);
	}
	public static WebDriver driver;
	
	@Parameters("sBrowser")
	@BeforeClass
	public static void openApplication(@Optional("chrome")String sbrowser)
	{
		if(sbrowser.equals("chrome"))
		{
		 driver=new ChromeDriver();
		}
		else if(sbrowser.equals("firefox"))
		{
			driver=new FirefoxDriver();
		}
		else
		{
			driver=new InternetExplorerDriver();	
		}
		driver.manage().timeouts().implicitlyWait(ITO,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		}
	
	@BeforeMethod
	public void openApp()
	{
		driver.get(URL);
		
	}
	
//	@AfterMethod
//	public static void closeApplication()
//	{
//		driver.close();
//	}
//	
}
